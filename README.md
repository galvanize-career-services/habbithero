# Getting Started


```bash
git clone git@gitlab.com:galvanize-career-services/habbithero.git
cd habbithero
git checkout -b <branch-name>
docker volume create node-modules
docker volume create external
docker volume create data
docker compose up --build
```

## Endpoints

[API: http://localhost:8000/docs](http://localhost:8000/docs)
[FE: http://localhost:5173](http://localhost:5173)
