import { useState, useEffect } from 'react'

import './App.css'

function App() {
  const [tasks, updateTasks] = useState([]);
  const [isLoading, updateIsLoading] = useState(true);
  const [err, updateErr] = useState(null);

  useEffect(() => {
    // TODO: Add call to get tasks
    fetch("http://localhost:8000/tasks")
      .then(data => data.json())
      .then(res => updateTasks(res))
      .catch(e => {
        updateErr(e.toString())
      })
      .finally(() => updateIsLoading(false))
  }, [])

  return (
    <>Hello, world! Foobar!</>
  )
}

export default App
