from fastapi import APIRouter, Depends
from fastapi.security import OAuth2PasswordRequestForm
from typing import Annotated
from datetime import timedelta
import os

from queries.auth import AuthQueries
from models.auth import (
    UserCreate,
    User,
    UserInDB,
    UserOutDB
)

from utils.authenticator import (
    get_password_hash,
    create_access_token,
    get_current_active_user
)

router = APIRouter()

@router.post("/token")
async def login(
    form_data: Annotated[OAuth2PasswordRequestForm, Depends()],
    queries: AuthQueries = Depends()
):
    user = queries.authenticate_user(form_data.username, form_data.password)
    if 'password' not in user:
        return { "message": "Username or password is incorrect"}
    access_token_expires = timedelta(int(os.environ["ACCESS_TOKEN_EXPIRE_MINUTES"]))
    access_token = create_access_token(
        data={"sub": user["username"]},
        expires_delta=access_token_expires
    )
    return {"access_token": access_token, "token_type": "Bearer"}

@router.post("/create")
async def create(
    user: UserCreate,
    queries: AuthQueries = Depends()
):
    hashed_password = get_password_hash(user.password)
    new_user = { **user.dict(), "hashed_password": hashed_password }
    del new_user['password']
    queries.create_user(user)

@router.get("/me")
async def read_me(
    current_user: Annotated[User, Depends(get_current_active_user)]
):
    return current_user
