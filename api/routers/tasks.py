from fastapi import APIRouter, Depends

from queries.tasks import TasksQueries

from models.tasks import Task

router = APIRouter()

@router.get("/")
def get_tasks(queries: TasksQueries = Depends()):
    return queries.get_tasks()

@router.post("/create")
def create_task(task: Task, queries: TasksQueries = Depends()):
    queries.create_task(task)
