fastapi[all]==0.109.0
pymongo[srv]==4.2.0
python-jose[cryptography]==3.3.0
passlib[bcrypt]==1.7.4
pydantic==2.3.0
