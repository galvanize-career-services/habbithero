from utils.client import Queries

from bson.json_util import dumps, loads

from models.auth import UserCreate, User, UserInDB, UserOutDB

class TasksQueries(Queries):
    DB_NAME = "taskrabbit"
    COLLECTION = "tasks"

    def get_tasks(self):
       tasks = self.collection.find()
       task_list = loads(dumps(tasks))

       for task in task_list:
            task["id"] = str(task["_id"])
            del task["_id"]
       return task_list

    def create_task(self, task):
        return self.collection.insert_one(task.dict())
