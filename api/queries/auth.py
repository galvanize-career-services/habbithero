from utils.client import Queries
from utils.authenticator import verify_password
from models.auth import UserCreate, User, UserInDB, UserOutDB

class AuthQueries(Queries):
    DB_NAME = "taskrabbit"
    COLLECTION = "accounts"

    def authenticate_user(self, username: str, password: str):
        user = self.get_user(username)
        if not user:
            return {"message": "Username or password is incorrect"}
        if 'hashed_password' in user:
            if not verify_password(password, user.hashed_password):
                return  {"message": "Username or password is incorrect"}
        return user

    def get_user(self, username: str):
        try:
            user = self.collection.find_one({"username": username})
            user["id"] = str(user["_id"])
            del user["_id"]
            return user
        except:
            return  {"message": "Username or password is incorrect"}

    def create_user(self, user: UserInDB):
        try:
            self.collection.insert_one(user.dict())
        except Exception:
            raise Exception()
