from pydantic import BaseModel
from datetime import datetime
from typing import List

class Step(BaseModel):
    name: str
    is_done: bool = False

class Task(BaseModel):
    name: str
    description: str | None
    steps: List[Step] | None
    dueDate: datetime | None
    is_done: bool = False

class TaskOut(Task):
    uuid: str
